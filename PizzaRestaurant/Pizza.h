//
//  Pizza.h
//  PizzaRestaurant
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Lighthouse Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PizzaSize) {
    PizzaSizeSmall,
    PizzaSizeMedium,
    PizzaSizeLarge
};

@interface Pizza : NSObject

+ (Pizza*)largePepperoni;
+ (Pizza*)meatLoversWithSize:(PizzaSize)size;

@property (nonatomic,readonly,assign) PizzaSize size;
@property (nonatomic,readonly,strong) NSArray<NSString*>* toppings;

- (instancetype)initWithSize:(PizzaSize)size toppings:(NSArray<NSString*>*)toppings;

@end
