//
//  Kitchen.m
//  PizzaRestaurant
//
//  Created by Steven Masuch on 2014-07-19.
//  Copyright (c) 2014 Lighthouse Labs. All rights reserved.
//

#import "Kitchen.h"

PizzaSize parseSize(NSString* sizeStr) {
    if ([sizeStr isEqualToString:@"small"]) {
        return PizzaSizeSmall;
    }
    if ([sizeStr isEqualToString:@"medium"]) {
        return PizzaSizeMedium;
    }
    if ([sizeStr isEqualToString:@"large"]) {
        return PizzaSizeLarge;
    }
    return PizzaSizeMedium;
}

@implementation Kitchen

- (Pizza *)makePizzaWithSize:(PizzaSize)size toppings:(NSArray *)toppings
{
    return [[Pizza alloc] initWithSize:size toppings:toppings];
}

- (Pizza *)handleOrder:(NSArray<NSString *> *)order
{
    NSString *pizzaSizeString = order[0]; // [order objectAtIndex:0]
    if ([pizzaSizeString isEqualToString:@"pepperoni"]) {
        return [Pizza largePepperoni];
    }

    PizzaSize size = parseSize(pizzaSizeString);

    NSArray<NSString*>* toppings = [order subarrayWithRange:NSMakeRange(1, order.count - 1)];

    if ([toppings[0] isEqualToString:@"meatlovers"]) {
        return [Pizza meatLoversWithSize:size];
    }

    return [self makePizzaWithSize:size toppings:toppings];
}

@end
