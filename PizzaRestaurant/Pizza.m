//
//  Pizza.m
//  PizzaRestaurant
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Lighthouse Labs. All rights reserved.
//

#import "Pizza.h"

NSString* unparseSize(PizzaSize size) {
    switch (size) {
        case PizzaSizeSmall:
            return @"small";
        case PizzaSizeMedium:
            return @"medium";
        case PizzaSizeLarge:
            return @"large";
    }
}

@implementation Pizza

+ (Pizza*)largePepperoni
{
    return [[Pizza alloc] initWithSize:PizzaSizeLarge
                              toppings:@[@"pepperoni"]];
}

+ (Pizza*)meatLoversWithSize:(PizzaSize)size
{
    return [[Pizza alloc] initWithSize:size
                              toppings:@[@"pepperoni", @"ham", @"goat"]];
}

- (instancetype)initWithSize:(PizzaSize)size toppings:(NSArray<NSString *> *)toppings
{
    self = [super init];
    if (self) {
        _size = size;
        _toppings = toppings;
    }
    return self;
}

// Totally don't need to do this! Just for displaying purposes
- (NSString*)description
{
    NSString* size = unparseSize(self.size);
    NSString* toppings = [self.toppings componentsJoinedByString:@", "];
    return [NSString stringWithFormat:@"%@ pizza with %@", size, toppings];
}

@end
